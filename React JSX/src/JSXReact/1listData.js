import React, { Component } from '../../node_modules/react';
//Khai báo data list
const list = [
  {
    title: 'React',
    uri: 'https://react.org',
    author: 'htp',
    num_comments: 3,
    points: 4,
    objectID: 0
  },
  {
    title: 'Mobx',
    uri: 'https://mobx.js.org',
    author: 'htp',
    num_comments: 3,
    points: 4,
    objectID: 0
  }
];

//export class render  JSX show
export default class ListDataJSX extends Component {
  render() {
    return (
      <div className="App">
        {/*Using .map()  thay thế  for-loop  get item in array
					 => không tốn resource  để rerender lại những item không thay đổi.
					 gán  array vào một   value mới để thay đổi và không ảnh hưởng tới giá trị  nguyên  bản
				*/}
        {list.map((item, key) => {
          // gán list=item  => map có thể nhận vào 3 giá trí: list item, index, item hiện tại
          return (
            <div key={key}>
              {/* key giúp React định nghĩa cái item  thay đổi, add, và remove , key phải là duy nhất */}
              <span>
                <a href={item.url}>{item.title}</a>
              </span>
              <span>{item.author}</span>
              <span>{item.num_comments}</span>
              <span>{item.points}</span>
            </div>
          );
        })}
      </div>
    );
  }
}
