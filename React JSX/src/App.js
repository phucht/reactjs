import React, { Component, PropTypes } from 'react';
import logo from './logo.svg';
import './App.css';
import JSX from './Lifecycle/1JSX';
import Structure from './Lifecycle/2structureReactJS';
import LSData from './JSXReact/1listData';

class App extends React.Component {
  constructor(props) {
    super(props); // sử dụng props trong phạm vi func contructor này;
    this.state = {
      test: false
    }; //Thiết lập state cho component
  }

  render() {
    return (
      <div>
        <JSX />
        <Structure />
        <LSData />
      </div>
    );
  }
}

export default App;
