import React, { Component, PropTypes } from 'react';

class LifeCycle extends Component {
  constructor(props) {
    super(props); // sử dụng props trong phạm vi func contructor này;
    this.state = {
      test: false
    }; //Thiết lập state cho component
  }

  componentWillMount() {
    // đc thực thi trước khi component render trên cả server side và client side
    console.log('componentWillMount');
  }

  componentDidMount() {
    //đc  khi component đc render trên client side.
    // tại đây AJAX đc request, DOM or update state đc thực thi.
    // method này đc sử dụng để connect JS Framework khác và  các function  với delay execution như setTimeout or setInerval
    console.log('componentDidMount');
    this.setState({ test: true });
  }

  componentWillUnmount() {
    //Hàm này thực hiện  1 lần duy nhất, khi component sẽ unmount
    //Hàm này sử dụng khi xóa các  timer không còn sử dụng
    console.log('componentWillUnmount');
  }

  componentWillReceiveProps() {
    // thực thi ngày khi props đc update trước khi  render  lại
    console.log('componentWillReceiveProps');
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('shouldComponentUpdate');
    return false;
    //kt   func render có update hay ko nếu ko update thì không cần render lại
    //return true => render lại
    //return false => ko cần render lại
  }

  componentWillUpdate(nextProps, nextState) {
    // Hàm này thực hiện dựa vào kết quả của hàm trên (shouldComponentUpdate)
    // Nếu hàm trên trả về false, thì React sẽ không gọi hàm này
    console.log('componentWillUpdate');
  }

  componentDidUpdate(prevProps, prevState) {
    // Hàm này thực hiện sau khi component được render lại, từ kết quả của componentWillUpdate
    console.log('componentDidUpdate');
  }

  render() {
    //
    return <div>DEF</div>;
  }

  static propTypes = {
    //Khai báo kiểu biến cho props
  };
  static defaultProps = {
    //Khai báo giá trị mặc định cho props
  };
}

export default LifeCycle;
