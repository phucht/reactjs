/* JSX  là JavaScript XML => syntax for Javascrip theo kiểu XML
	Trong React  JSX dùng để thay thế  React.createElement
		ex: 
			React syntax : React.createElement(
					"div",
					{
						className:"element"
					},
					"Welcome to JSX"
				);
			JSX syntax:   const element =<div class="example">Welcome to JSX</div>
	JSX dùng để  View giống template trong HTML
	Có thể dùng Javascrip expression(biểu thức)

*/

import React, { Component } from 'react';
// JSX react sử dụng ES6
export default class JSX extends Component {
  /* const & let in ES6
			const: dùng cho  giá trị bất biến không đổi
			let: dùng   cho giá trị có thể  change
			hạn chế dùng var	
	*/

  render() {
    const v = 'Hello';
    let i = 1;
    return (
      // dùng JS Expression trong dấu{}
      <div>
        <h2>{v}</h2>
        <h1>{i == 1 ? 'JSX' : 'Not JXS '}</h1>
      </div> // biểu thử đc sử dụng trong dấu {}
    );
  }
}
